package de.unity.packagy.model;

import lombok.Builder;
import lombok.Data;

/**
 * Representation of items put in packages
 */
@Data
@Builder
public class Item implements Comparable<Item>{
    private Integer id;
    private Double weight;
    private Double price;
    private String currency;

    @Override
    public int compareTo(Item other) {
        double value = this.price/this.weight;
        double otherValue = other.price/other.weight;
        if (value == otherValue ) {
            return 0;
        }
        if (value > otherValue) {
            return 1;
        }
        else return -1;
    }
}
