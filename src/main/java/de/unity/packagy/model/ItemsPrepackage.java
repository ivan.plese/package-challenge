package de.unity.packagy.model;

import lombok.Data;

import java.util.List;
/**
 * Container that holds data form input file.
 */
@Data
public class ItemsPrepackage {
    private Double maxWeight;
    private List<Item> items;
}
