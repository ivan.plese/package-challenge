package de.unity.packagy.exception;

public class PackageConstraintsException extends RuntimeException {
    public PackageConstraintsException(String message) {
        super(message);
    }
}
