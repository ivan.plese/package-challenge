package de.unity.packagy.exception;

public class FileReaderException extends RuntimeException{
    public FileReaderException(String message) {
        super(message);
    }
}
