package de.unity.packagy.exception;

public class PackageDetailsException extends RuntimeException{
    public PackageDetailsException(String message) {
        super(message);
    }
}
