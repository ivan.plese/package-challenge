package de.unity.packagy.configuration;

public class Constants {
    private Constants() {}
    public static final String SEMICOLON = ":";
    public static final String OPEN_BRACETS_AND_SPACE_REGEX = "[\\s(]";
    public static final String CLOSING_BRACETS = "\\)";
    public static final String COMMA = ",";
}
