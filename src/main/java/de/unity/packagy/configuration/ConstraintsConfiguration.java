package de.unity.packagy.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(value = "app.constraints")
public class ConstraintsConfiguration {
    private Double packageMaxWeight;
    private Integer packageMaxItems;
    private Double itemMaxWeight;
    private Double itemMaxCost;
}
