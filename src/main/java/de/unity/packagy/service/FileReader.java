package de.unity.packagy.service;

import de.unity.packagy.model.ItemsPrepackage;
import de.unity.packagy.exception.FileReaderException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@AllArgsConstructor
public class FileReader {

    private final PackageBuilder packageBuilder;

    /**
     *  Reads the file from path holding the data on max weight of package and items available
     *
     * @param inputPath path from command line argument
     * @return Items populated from input file
     */
    public List<ItemsPrepackage> readInputFile(Path inputPath) {
        File inputFile = inputPath.toFile();
        List<ItemsPrepackage> itemsPrepackageList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new java.io.FileReader(inputFile))){
            reader.lines()
                    .filter(line -> StringUtils.hasLength(line))
                    .forEach(line -> {
                        if (StringUtils.hasLength(line)) {
                            ItemsPrepackage packageItem = packageBuilder.createPackage(line);
                            itemsPrepackageList.add(packageItem);
                        }
                    });
        } catch (FileNotFoundException fileNotFoundException) {
            log.error("File {} not found", inputFile, fileNotFoundException);
            throw new FileReaderException("Input file not found");
        } catch (IOException ioException) {
            log.error("Error reading input file {}", inputPath, ioException);
            throw new FileReaderException("Error reading file");
        }
        log.debug("Found {} package details", itemsPrepackageList.size());
        return itemsPrepackageList;
    }
}
