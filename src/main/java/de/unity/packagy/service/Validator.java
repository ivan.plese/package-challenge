package de.unity.packagy.service;

import de.unity.packagy.configuration.ConstraintsConfiguration;
import de.unity.packagy.exception.PackageConstraintsException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Validator for package and item constraints.
 */
@Component
@AllArgsConstructor
public class Validator {
    private final ConstraintsConfiguration constraints;

    public void validatePackageMaxWeight(Double value) {
        if (value > constraints.getPackageMaxWeight()) {
            throw new PackageConstraintsException(
                    "Max package weight is " + constraints.getPackageMaxWeight() + " but was " + value);
        }
    }

    public void validatePackageMaxItems(Integer value) {
        if (value > constraints.getPackageMaxItems()) {
            throw new PackageConstraintsException(
                    "Max items in package is " + constraints.getPackageMaxItems() + " but was " + value);
        }
    }

    public void validateItemMaxWeight(Double value) {
        if (value > constraints.getItemMaxWeight()) {
            throw new PackageConstraintsException(
                    "Max item weight is " + constraints.getItemMaxWeight() + " but was " + value);
        }
    }

    public void validateItemMaxCost(Double value) {
        if (value > constraints.getItemMaxCost()) {
            throw new PackageConstraintsException(
                    "Max item cost is " + constraints.getItemMaxCost() + " but was " + value);
        }
    }
}
