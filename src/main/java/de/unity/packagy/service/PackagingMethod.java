package de.unity.packagy.service;

import de.unity.packagy.model.ItemsPrepackage;

import java.util.Optional;

/**
 * Interface for different implementations of packaging
 */
public interface PackagingMethod {
    Optional<String> doPackage(ItemsPrepackage itemsPrepackage);
}
