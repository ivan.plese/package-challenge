package de.unity.packagy.service;

import de.unity.packagy.model.Item;
import de.unity.packagy.model.ItemsPrepackage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Processes container holding items and delivers the output to console line.
 * Holds concrete implementation of packaging method.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class PackagingService {

    private final PackagingMethod dynamicProgramingPackagingMethod;

    /**
     * Processes list of pre-packaged containers
     * @param itemsPrepackages list of pre-packages items
     */
    public void processPackages(List<ItemsPrepackage> itemsPrepackages) {
        log.info("###################################");
        log.info("Results for optimal packaging: ");
        for (ItemsPrepackage itemsPrepackage : itemsPrepackages) {
            Optional<String> result = dynamicProgramingPackagingMethod.doPackage(itemsPrepackage);
            printResult(result);
        }
        log.info("###################################");
    }

    /**
     * Using System.out to print results resembling required output in assignment
     * @param result
     */
    private void printResult(Optional<String> result) {
        if (result.isPresent()) {
            System.out.println(result.get());
        } else {
            System.out.println("-");
        }
    }
}
