package de.unity.packagy.service;

import de.unity.packagy.model.Item;
import de.unity.packagy.model.ItemsPrepackage;
import de.unity.packagy.exception.PackageDetailsException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static de.unity.packagy.configuration.Constants.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class PackageBuilder {

    private final Validator validator;

    /**
     * Creates containter that holds items and package max weight
     *
     * @param line single line form input file
     * @return Item container
     */
    public ItemsPrepackage createPackage(String line) {
        ItemsPrepackage result = new ItemsPrepackage();
        String[] maxPackageWeightAndItems = line.split(SEMICOLON);
        if (maxPackageWeightAndItems.length !=  2) {
            throw new PackageDetailsException("Package details " + maxPackageWeightAndItems + " have "
                    + maxPackageWeightAndItems.length + " parts, should be 2");
        }
        Double maxPackageWeight = getMaxPackageWeight(maxPackageWeightAndItems[0]);
        List<Item> items = createItemList(maxPackageWeightAndItems[1]);
        result.setMaxWeight(maxPackageWeight);
        result.setItems(items);
        log.debug("Created package with {} items and max weight {}", items.size(), maxPackageWeight);
        return result;
    }

    private Double getMaxPackageWeight(String maxPackageWeightValue) {
        Double maxPackageWeight;
        try {
            maxPackageWeight = Double.parseDouble(maxPackageWeightValue.trim());
        } catch (NumberFormatException e) {
            log.error("Error parsing max package weight {}", maxPackageWeightValue, e.getMessage());
            throw new PackageDetailsException("Could not read package max weight value " + maxPackageWeightValue);
        }
        validator.validatePackageMaxWeight(maxPackageWeight);
        return maxPackageWeight;
    }

    private List<Item> createItemList(String items) {
        items = items.replaceAll(OPEN_BRACETS_AND_SPACE_REGEX, "");
        String[] itemsSplit = items.split(CLOSING_BRACETS);
        validator.validatePackageMaxItems(itemsSplit.length);
        return Arrays.stream(itemsSplit)
                .map(this::createItem)
                .collect(Collectors.toList());
    }

    private Item createItem(String item) {
        String[] itemDetails = item.split(COMMA);
        if(itemDetails.length != 3) {
            throw new PackageDetailsException("Item details " + item + " have "
                    + itemDetails.length + " parts, should be 3");
        }
        String currency = itemDetails[2].substring(0, 2);
        Double weight;
        Double price;
        Integer number;
        try {
            number = Integer.parseInt(itemDetails[0]);
            weight = Double.parseDouble(itemDetails[1]);
            price = Double.parseDouble(itemDetails[2].substring(1));
        } catch (NumberFormatException e) {
            log.error("Error parsing values {} {} {}", itemDetails[0], itemDetails[1], itemDetails[2].substring(1), e);
            throw new PackageDetailsException("Could not parse values " + itemDetails[1] + ", "
                    + itemDetails[1] + ", " + itemDetails[2].substring(1));
        }
        validator.validateItemMaxWeight(weight);
        validator.validateItemMaxCost(price);
        log.debug("Crating item no. {} with weight {} price {} and currency {}", number, weight, price, currency);
        return Item.builder().id(number).currency(currency).weight(weight).price(price).build();
    }
}
