package de.unity.packagy.service;

import de.unity.packagy.model.Item;
import de.unity.packagy.model.ItemsPrepackage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Packaging solution using Dynamic Programming solution with two tables,
 * one holding the result and the other holding the item names whose sum of prices is the highest possible value
 */
@Slf4j
@Component
public class DynamicProgramingPackagingMethod implements PackagingMethod{

    /**
     * Method for packaging with solution concentrated on low time complexity
     *
     * @param itemsPrepackage holds items and max wight
     * @return list of items that hold best weight/value ratio
     */
    @Override
    public Optional<String> doPackage(ItemsPrepackage itemsPrepackage) {
        int maxWeightFinal = itemsPrepackage.getMaxWeight().intValue() + 1;
        int numberOfItems = itemsPrepackage.getItems().size() + 1;
        List<Item> items = itemsPrepackage.getItems();

        double[][] matrix = new double[numberOfItems][maxWeightFinal];
        String[][] matrixOfIncluded = new String[numberOfItems][maxWeightFinal];

        for (int i = 1; i < numberOfItems; i++) {
            Item item = items.get(i - 1);
            for (int j = 1; j < maxWeightFinal; j++) {
                if (item.getWeight() > j) {
                    matrix[i][j] = matrix[i - 1][j];
                    matrixOfIncluded[i][j] = getPrevious(matrixOfIncluded, matrix, i, j);
                } else {
                    double previousRow = matrix[i - 1][j];
                    double currentItemIncluded = matrix[i - 1][j - item.getWeight().intValue()] + item.getPrice();
                    double max = Math.max(previousRow, currentItemIncluded);
                    matrix[i][j] = max;
                    if (max == currentItemIncluded) {
                        matrixOfIncluded[i][j] = getIncludedSolutions(item, matrixOfIncluded, matrix, i, j);
                    } else {
                        matrixOfIncluded[i][j] = matrixOfIncluded[i-1][j];
                    }
                    matrix[i][j] = max;
                }
            }
        }
        return Optional.ofNullable(matrixOfIncluded[numberOfItems-1][maxWeightFinal-1]);
    }

    /**
     * Inserts the values in matrixOfIncluded when current item is included.
     *
     * @param item current item
     * @param matrixOfIncluded matrix holding IDs of items
     * @param matrix matrix holding max values
     * @param i current row
     * @param j current column
     * @return
     */
    private String getIncludedSolutions(Item item, String[][] matrixOfIncluded, double[][] matrix, int i, int j) {
        double previousValue = matrix[i - 1][j - item.getWeight().intValue()];
        String previousItemNumber = matrixOfIncluded[i - 1][j - item.getWeight().intValue()];
        if (previousItemNumber != null  && previousValue > 0) {
            return previousItemNumber + "," + item.getId();
        }
        return item.getId().toString();
    }

    /**
     * Inserts the values in matrixOfIncluded when current item is NOT included.
     * Only item IDs from previous rows will be copied.
     *
     * @param matrixOfIncluded
     * @param matrix
     * @param i
     * @param j
     * @return
     */
    private String getPrevious(String[][] matrixOfIncluded, double[][] matrix, int i, int j) {
        double previousValue = matrix[i-1][j];
        String previousItemNumber = matrixOfIncluded[i-1][j];
        if (previousItemNumber != null && previousValue > 0) {
            return previousItemNumber;
        }
        return null;
    }
}
