package de.unity.packagy;

import de.unity.packagy.model.ItemsPrepackage;
import de.unity.packagy.service.FileReader;
import de.unity.packagy.service.PackagingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.util.Objects.requireNonNull;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties
@RequiredArgsConstructor
public class PackagingApplication implements ApplicationRunner {

    private final FileReader fileReader;
    private final PackagingService packagingService;

    public static void main(String[] args) {
        SpringApplication.run(PackagingApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) {
        if(args.getSourceArgs().length < 1) {
            log.warn("No input file provided. Command line argument with path to input file is required to process packages");
        } else {
            Path inputPath = Paths.get(args.getSourceArgs()[0]);
            log.debug("Path argument: {}", inputPath.toAbsolutePath());
            List<ItemsPrepackage> itemsPrepackages = fileReader.readInputFile(inputPath);
            packagingService.processPackages(itemsPrepackages);
        }
    }
}
