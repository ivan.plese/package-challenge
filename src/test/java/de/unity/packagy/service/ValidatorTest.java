package de.unity.packagy.service;

import de.unity.packagy.configuration.ConstraintsConfiguration;
import de.unity.packagy.exception.PackageConstraintsException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValidatorTest {
    private static Validator validator;
    @BeforeAll
    private static void setup() {
        ConstraintsConfiguration config = new ConstraintsConfiguration();
        config.setPackageMaxItems(15);
        config.setPackageMaxWeight(100d);
        config.setItemMaxWeight(100d);
        config.setItemMaxCost(100d);
        validator = new Validator(config);
    }

    @Test
    void validatePackageMaxWeight_maxWeightExceeded_throwsException() {
        PackageConstraintsException thrown = assertThrows(
                PackageConstraintsException.class,
                () -> validator.validatePackageMaxWeight(101d),
                "Expected validatePackageMaxWeight to throw, but not thrown"
        );
        assertTrue(thrown.getMessage().contains(" but was "));
    }

    @Test
    void validatePackageMaxWeight_maxWeightNotExceeded_noException() {
        validator.validatePackageMaxWeight(100d);
    }

    @Test
    void validatePackageMaxItems_maxItemsExceeded_throwException() {
        PackageConstraintsException thrown = assertThrows(
                PackageConstraintsException.class,
                () -> validator.validatePackageMaxItems(16),
                "Expected validatePackageMaxItems to throw, but not thrown"
        );
        assertTrue(thrown.getMessage().contains(" but was "));
    }

    @Test
    void validatePackageMaxItems_maxItemsNotExceeded_noException() {
        validator.validatePackageMaxItems(15);
    }

    @Test
    void validateItemMaxWeight_maxItemWeightExceeded_throwException() {
        PackageConstraintsException thrown = assertThrows(
                PackageConstraintsException.class,
                () -> validator.validateItemMaxWeight(101d),
                "Expected validateItemMaxWeight to throw, but not thrown"
        );
        assertTrue(thrown.getMessage().contains(" but was "));
    }

    @Test
    void validateItemMaxWeight_maxItemWeightNotExceeded_noException() {
        validator.validateItemMaxWeight(100d);
    }

    @Test
    void validateItemMaxCost_maxItemCostExceeded_throwException() {
        Double value = Double.parseDouble("101");
        PackageConstraintsException thrown = assertThrows(
                PackageConstraintsException.class,
                () -> validator.validateItemMaxCost(value),
                "Expected validateItemMaxCost to throw, but not thrown"
        );
        assertTrue(thrown.getMessage().contains(" but was "));
    }

    @Test
    void validateItemMaxCost_maxItemCostNotExceeded_noException() {
        validator.validateItemMaxCost(100d);
    }
}