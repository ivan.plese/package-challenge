package de.unity.packagy.service;

import de.unity.packagy.configuration.ConstraintsConfiguration;
import de.unity.packagy.model.ItemsPrepackage;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

@Slf4j
class DynamicProgramingPackagingMethodTest {


    private PackagingMethod packagingMethod = new DynamicProgramingPackagingMethod();
    private static PackageBuilder packageBuilder;

    @BeforeAll
    private static void setup() {
        ConstraintsConfiguration config = new ConstraintsConfiguration();
        config.setPackageMaxItems(15);
        config.setPackageMaxWeight(100d);
        config.setItemMaxWeight(100d);
        config.setItemMaxCost(100d);
        Validator validator = new Validator(config);
        packageBuilder = new PackageBuilder(validator);
    }

    @Test
    public void doPackage_firstSetOfItems_expectedResult() {
        ItemsPrepackage aPackage = packageBuilder.createPackage("5 : (1,5,€60) (2,3,€50) (3,4,€70) (4,2,€30) ");
        Optional<String> result = packagingMethod.doPackage(aPackage);
        Assertions.assertEquals("2,4", result.get());
    }

    @Test
    public void doPackage_secondSetOfItems_expectedResult() {
        ItemsPrepackage aPackage = packageBuilder.createPackage("75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)");
        Optional<String> result = packagingMethod.doPackage(aPackage);
        Assertions.assertEquals("2,7", result.get());
    }

}