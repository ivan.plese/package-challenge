package de.unity.packagy.service;

import de.unity.packagy.configuration.ConstraintsConfiguration;
import de.unity.packagy.exception.PackageDetailsException;
import de.unity.packagy.model.ItemsPrepackage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ItemsPrepackageBuilderTest {

    private static PackageBuilder packageBuilder;

    @BeforeAll
    private static void setup() {
        ConstraintsConfiguration config = new ConstraintsConfiguration();
        config.setPackageMaxItems(15);
        config.setPackageMaxWeight(100d);
        config.setItemMaxWeight(100d);
        config.setItemMaxCost(100d);
        Validator validator = new Validator(config);
        packageBuilder = new PackageBuilder(validator);
    }

    @Test
    void createPackage_validInput_returnsPackage() {
        ItemsPrepackage itemsPrepackage = packageBuilder.createPackage("81 : (1,53.38,€45) ");

        Assertions.assertEquals(81d, itemsPrepackage.getMaxWeight());
        Assertions.assertEquals(1, itemsPrepackage.getItems().size());
        Assertions.assertEquals(53.38d, itemsPrepackage.getItems().get(0).getWeight());
        Assertions.assertEquals(45d, itemsPrepackage.getItems().get(0).getPrice());
    }

    @Test
    void createPackage_invalidInput_throwsError() {
        PackageDetailsException thrown = assertThrows(
                PackageDetailsException.class,
                () -> packageBuilder.createPackage("81 : (1,53.38 "),
                "Expected createPackage to throw, but not thrown"
        );
        assertTrue(thrown.getMessage().contains(" parts, should be 3"));
    }

    @Test
    void createPackage_invalidPackageMaxWeight_throwsError() {
        PackageDetailsException thrown = assertThrows(
                PackageDetailsException.class,
                () -> packageBuilder.createPackage(": (1,53.38 "),
                "Expected createPackage to throw, but not thrown"
        );
        assertTrue(thrown.getMessage().contains("Could not read package max weight value"));
    }
}