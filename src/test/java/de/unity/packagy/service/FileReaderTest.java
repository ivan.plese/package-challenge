package de.unity.packagy.service;

import de.unity.packagy.exception.PackageConstraintsException;
import de.unity.packagy.model.ItemsPrepackage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class FileReaderTest {

    @Autowired
    private FileReader fileReader;

    private String sampleInput = "sampleInput.txt";
    private String exceedsMaxItems  = "exceedsMaxItems.txt";
    private String exceedsItemWeight = "exceedsItemWeight.txt";
    private String exceedsItemPrice = "exceedsItemPrice.txt";

    @Test
    void readInputFile_sampleInput_packagesProcessed() throws IOException {
        File inputPath = new ClassPathResource(sampleInput).getFile();
        List<ItemsPrepackage> itemsPrepackages = fileReader.readInputFile(Paths.get(inputPath.getAbsolutePath()));
        Assertions.assertEquals(4, itemsPrepackages.size());
    }

    @Test
    void readInputFile_exceedsMaxItems_throwsError() throws IOException {
        File inputPath = new ClassPathResource(exceedsMaxItems).getFile();
        PackageConstraintsException thrown = assertThrows(
                PackageConstraintsException.class,
                () -> fileReader.readInputFile(Paths.get(inputPath.getAbsolutePath())),
                "Expected readInputFile to throw, but not thrown"
        );
        assertTrue(thrown.getMessage().contains("Max items in package is"));
    }

    @Test
    void readInputFile_exceedsMaxItemWeight_throwsError() throws IOException {
        File inputPath = new ClassPathResource(exceedsItemWeight).getFile();
        PackageConstraintsException thrown = assertThrows(
                PackageConstraintsException.class,
                () -> fileReader.readInputFile(Paths.get(inputPath.getAbsolutePath())),
                "Expected readInputFile to throw, but not thrown"
        );
        assertTrue(thrown.getMessage().contains("Max item weight is"));
    }

    @Test
    void readInputFile_exceedsMaxItemPrice_throwsError() throws IOException {
        File inputPath = new ClassPathResource(exceedsItemPrice).getFile();
        PackageConstraintsException thrown = assertThrows(
                PackageConstraintsException.class,
                () -> fileReader.readInputFile(Paths.get(inputPath.getAbsolutePath())),
                "Expected readInputFile to throw, but not thrown"
        );
        assertTrue(thrown.getMessage().contains("Max item cost is "));
    }
}